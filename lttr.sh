#!/usr/sbin/bash

######"listen-to-the-radio.sh" is a member of the //Quantifier//Consortium//. All rights reserved.

bold=$(tput bold)
natural=$(tput sgr0)
comment0="#EXTM3U"
comment1="##https://gitlab.com/royaleconqueso/Lttr"
comment2="##Config file for the listen-to-the-radio music player $(date)"
red=`tput setaf 1`
reset=`tput sgr0`
#1="1"
#2="2"
##two separate comments is annoying, but necessary, since not all versions of echo respect the newline parameter, or the -e flag

if ! [ -x "$(command -v mpv)" ]; then
  echo "${bold}Oh, no! This script requires that you have mpv installed and available in your path. You should either install mpv, or edit the script to include your preferred player. ${natural}Exiting..." >&2
  exit 1
fi

freestylenum=$(( $RANDOM % 6000 ))

mkdir -m700 -p /home/$USER/.config/lttr/
touch /home/$USER/.config/lttr/lttrrc;
chmod +x /home/$USER/.config/lttr/lttrrc;
clear;

# https://www.leskanet.net/audio_outside.php
#https://wiki.bash-hackers.org/howto/getopts_tutorial
#https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
#https://www.baeldung.com/linux/convert-script-into-binary /usr/bin/shc -vrf lttr.sh -o lttr

if pgrep -x "mpv" > /dev/null
then
    echo "    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
    echo "    mpv is currently running. Attempting to gently kill"
    echo "    all processes containing mpv, to avoid noise"
    echo "    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
    killall mpv
else
    echo ""
fi

if pgrep -x "mplayer" > /dev/null
then
    echo "    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
    echo "    mplayer is currently running. Attempting to gently kill"
    echo "    all processes containing mplayer, to avoid noise"
    echo "    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
    killall mplayer
else
    echo ""
fi

echo -ne  "${red}    ____________________________	\n"
echo -ne  "   |............................|	\n"
echo -ne  "   |: gitlab.com/royaleconqueso:|	 \n"
echo -ne  "   |:       ${reset}  "LttR.sh" ${red}         :|	 \n"
echo -ne  "   |:     ,-.   _____   ,-.    :|	 \n"
echo -ne  "   |:    ( \`)) |_____| ( \`))   :|	\n"
echo -ne  "   |:     \`-\`   ' ' '   \`-\`    :| Which college rock \n"
echo -ne  "   |:     ,______________.     :| or NPR news station \n"
echo -ne  "   |...../::::o::::::o::::\.....| would you like to \n"
echo -ne  "   |..../:::O::::::::::O:::\....| listen to?\n"
echo -ne  "   |__/ /====/ /=//=/ /====/____/	\n\n ${reset}"

while getopts :s: flag
do
    case "${flag}" in
        s) username=${OPTARG};;
    esac
done
if [[ $username == ""  ]]; then

read -n 1 -p "
1=WVFS (Tallahassee)            2=WTUL (New Orleans)
3=WNYC (NPR New York)           4=WWNO (NPR New Orleans)
5=KEXP (Seatle)                 6=DR P6 Beat (Denmark)
7=Dandelion Radio (London)	8=KVRX (Austin)
9=WFUV (Bronx, NY)              0=Last Station Played
q=quit
==> ? " answer;

clear;

echo ""
echo " -- Happy Listening, $(whoami). The time and date right now is $(date) -- "

case $answer in

     0)

                echo " -- LttR is now happily playing for you the last station you listened to --";
                echo " --                  (Brought to you by Carl's Jr)                       --";
		echo " ";
		mpv --pulse-buffer=500 --cache=yes --cache-pause --input-ipc-server=/tmp/mpvsocket --playlist=/home/$USER/.config/lttr/lttrrc;;
      1)
		echo " -- Now you're listinging to WVFS, Tallahassee FL. You don't have to, but you are -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WVFS Tallahassee" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://voice.wvfs.fsu.edu:8000/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=600 --cache=yes --cache-pause http://voice.wvfs.fsu.edu:8000/stream --input-ipc-server=/tmp/mpvsocket;;
      2)
		echo " -- Now you're listinging to WTUL -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WTUL New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://129.81.255.83:8000/stream.m3u" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://129.81.255.83:8000/stream.m3u --input-ipc-server=/tmp/mpvsocket;;
      3)
		echo " -- Now you're listinging to your local NPR station WNYC -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WNYC New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://fm939.wnyc.org/wnycfm" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://fm939.wnyc.org/wnycfm --input-ipc-server=/tmp/mpvsocket;;
      4)
		echo " -- Now you're listinging to your local NPR station WWNO -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WWNO New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://tektite.streamguys1.com:5145/wwnolive" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://tektite.streamguys1.com:5145/wwnolive --input-ipc-server=/tmp/mpvsocket;;
      5)
		echo " -- Now you're listinging to KEXP, Seatle -- ";
		echo " -- Attempting to skip the commercials. Please hold...";
		#while true; do echo -n .; sleep 1; done &
		#trap 'kill $1' SIGTERM SIGKILL;
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KEXP Seatle" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://kexp-mp3-128.streamguys1.com/kexp128.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --start=18 --force-seekable=yes --pulse-buffer=1000 --cache=yes --cache-pause https://kexp-mp3-128.streamguys1.com/kexp128.mp3 --input-ipc-server=/tmp/mpvsocket;;
 		############### https://kexp-mp3-128.streamguys1.com/kexp128.mp3
      6)
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
		echo " -- Now you're listinging to DR P6 all the way from Denmark -- ";
		echo " ";
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, DR P6 Denmark" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://live-icy.gslb01.dr.dk:80/A/A29H.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://live-icy.gslb01.dr.dk:80/A/A29H.mp3 --input-ipc-server=/tmp/mpvsocket;;
      d|D)
		echo " -- Now you're listinging to WUOG. Who here is missing that Golden Bowl?  -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WUOG Athens" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stream.wuog.org:8000/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stream.wuog.org:8000/stream --input-ipc-server=/tmp/mpvsocket;;
      y|Y)
		echo " -- Now you're listinging to WNYU. https://wnyu.org/schedule  -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WNYU New York University" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://cinema.acs.its.nyu.edu:8000/wnyu128.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://cinema.acs.its.nyu.edu:8000/wnyu128.mp3 --input-ipc-server=/tmp/mpvsocket;;
      u|U)
		echo " -- Now you're listinging to underground 80s on SomaFM. -- ";
		echo " -- Donate at https://somafm.com/u80s/  -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, SomaFM: Underground 80s (#1): Early 80s UK Synthpop and a bit of New Wave." >> /home/$USER/.config/lttr/lttrrc;
                #echo "https://somafm.com/u80s.pls --input-ipc-server=/tmp/mpvsocket" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://somafm.com/u80s.pls" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://somafm.com/u80s.pls --input-ipc-server=/tmp/mpvsocket;;
      v|V)
		echo " -- Now you're listinging to WVUM in Miami. -- ";
		echo " -- Un estacion secreto ! - ¿ Que T'Al ?  -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WVUM Miami https://www.wvum.org/listen" >> /home/$USER/.config/lttr/lttrrc;
                #echo "https://somafm.com/u80s.pls --input-ipc-server=/tmp/mpvsocket" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://s7.voscast.com:8693/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://s7.voscast.com:8693/stream --input-ipc-server=/tmp/mpvsocket;;
      8)
		echo " -- Now you're listinging to KVRX, Staying weird in Austin TX -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KVRX Austin" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream --input-ipc-server=/tmp/mpvsocket;;
      9)
		echo " -- Now you're listinging to WFUV, Straight Outta The Bronx! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WFUV New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://music.wfuv.org/music-hi" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --start=15 --force-seekable=yes --pulse-buffer=999 --cache=yes --cache-pause https://music.wfuv.org/music-hi --input-ipc-server=/tmp/mpvsocket;;
      s|S)
		echo " -- This is WSOU; Seton Hall's Pirate Radio. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WSOU New Jersey" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://crystalout.surfernetwork.com:8001/WSOU_MP3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://crystalout.surfernetwork.com:8001/WSOU_MP3 --input-ipc-server=/tmp/mpvsocket;;
      7)
		echo " -- This is Dandelion Radio. A LttR station from London! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, Dandelio Radio London" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stream.dandelionradio.com:9414" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stream.dandelionradio.com:9414 --input-ipc-server=/tmp/mpvsocket;;
      p|P)
		echo " -- This is P6 Beat from Denmark. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, P6 Beat Denmark" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://live-icy.gslb01.dr.dk/A/A29H.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://live-icy.gslb01.dr.dk/A/A29H.mp3 --input-ipc-server=/tmp/mpvsocket;;
      g|G)
		echo " -- This is WGRE out of Greencastle IN. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WGRE 91.5 FM and www.wgre.org" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://99.198.118.250:8387/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://99.198.118.250:8387/stream --input-ipc-server=/tmp/mpvsocket;;
      c|C)
		echo " -- This is CJLO 1690 AM Montreal. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, CJLO 1690 AM Montreal" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://rosetta.shoutca.st:8883/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://rosetta.shoutca.st:8883/stream --input-ipc-server=/tmp/mpvsocket;;
      n|N)
		echo " -- This is DFM from the Netherlands (dfm.nl). A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, DFM Stereo Amsterdam" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stereo.dfm.nu" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stereo.dfm.nu --input-ipc-server=/tmp/mpvsocket;;
      b|B)
		echo " -- This is the BBC World Service.. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, BBC World Service" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stream.live.vc.bbcmedia.co.uk/bbc_world_service" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stream.live.vc.bbcmedia.co.uk/bbc_world_service --input-ipc-server=/tmp/mpvsocket;;
      r|R)
		echo " -- Here's a 90's Hip-Hop LoFi remix. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, 90's Hip_Hop Remix" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://www.youtube.com/watch?v=pKKIxeMIv78 --no-video" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv https://www.youtube.com/watch?v=pKKIxeMIv78 --no-video;;
      z|Z)
		echo " -- Here's a the full album Reign in Blood. A Secret LttR Slayer station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, Slayer: Reign in Blood" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://youtu.be/nlPHYNG6ZF4?si=4Er5X5x7v4n2f_ws --no-video" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv https://youtu.be/nlPHYNG6ZF4?si=4Er5X5x7v4n2f_ws --no-video;;
      x|X)
		echo " -- Here's some 80s-90s Freestyle Dance Club Music. I was there! A Secret LttR dance station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, Freestyle 80s 90s Dance Club" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://youtu.be/_eqP_3iGBGo?si=bEi11gfypc5QiBIn --no-video" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --start=$freestylenum https://youtu.be/_eqP_3iGBGo?si=bEi11gfypc5QiBIn --no-video;;
      k|K)
		echo " -- Here's a the full Single's Album. A Secret LttR Bikini Kill station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, Bikini Kill: The Singles" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://www.youtube.com/channel/UCAQNGukCiNGtrTXGRv4M97A --no-video" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv https://www.youtube.com/channel/UCAQNGukCiNGtrTXGRv4M97A --no-video;;
      m|M)
		echo " -- Here's a the full audio of Minor Threat's live concert at the 9:30 club in 1983. A Secret LttR station! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, Minor Thread: Live At 9:30 Club, Washington DC, 1983-06-23" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://youtu.be/FpJoeXZxpqQ --no-video" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --start=141 https://youtu.be/FpJoeXZxpqQ?si=DchRZiYM2ZkPJTyH --no-video;;
      t|T)
                /usr/bin/telnet towel.blinkenlights.nl;;
      w|W)
		echo " -- This is a playlist of local writing music. A Secret LttR station!                      -- ";
		echo " -- Did it fail for you? It's looking for local music saved as ~/data/.music/*mp3 -- ";
                echo " ";

                mpv --shuffle ~/data/.music/*mp3;;
esac

elif [ "$username" = 1 ]; then
 echo " -- First Flag: Now you're listinging to WVFS, Tallahassee FL. You don't have to, but you are -- ";
                echo " ";
                echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "#EXTINF:-1, WVFS Tallahassee" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://voice.wvfs.fsu.edu:8000/stream" >> /home/$USER/.config/lttr/lttrrc;
                exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://voice.wvfs.fsu.edu:8000/stream --input-ipc-server=/tmp/mpvsocket;
elif [ "$username" = 2 ]; then
  echo " -- Now you're listinging to WTUL -- ";
                echo " ";
                echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "#EXTINF:-1, WTUL New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://129.81.255.83:8000/stream.m3u" >> /home/$USER/.config/lttr/lttrrc;
                exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://129.81.255.83:8000/stream.m3u --input-ipc-server=/tmp/mpvsocket;
elif [ "$username" = 3 ]; then
                echo " -- Now you're listinging to your local NPR station WNYC -- ";
                echo " ";
                echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "#EXTINF:-1, WNYC New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://fm939.wnyc.org/wnycfm" >> /home/$USER/.config/lttr/lttrrc;
                exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://fm939.wnyc.org/wnycfm --input-ipc-server=/tmp/mpvsocket;
elif [ "$username" = 4 ]; then
echo " -- Now you're listinging to your local NPR station WWNO -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WWNO New Orleans" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://tektite.streamguys1.com:5145/wwnolive" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://tektite.streamguys1.com:5145/wwnolive --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 5 ]; then
echo " -- Now you're listinging to KEXP, Seatle -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KEXP Seatle" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://kexp-mp3-128.streamguys1.com/kexp128.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://kexp-mp3-128.streamguys1.com/kexp128.mp3 --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 6 ]; then
echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
		echo " -- Now you're listinging to DR P6 all the way from Denmark -- ";
		echo " ";
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, DR P6 Denmark" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://live-icy.gslb01.dr.dk:80/A/A29H.mp3" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://live-icy.gslb01.dr.dk:80/A/A29H.mp3 --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 7 ]; then
echo " -- Now you're listinging to WUOG. Who here is missing that Golden Bowl?  -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WUOG Athens" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://stream.wuog.org:8000/stream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://stream.wuog.org:8000/stream --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 8 ]; then
echo " -- Now you're listinging to KVRX, staying weird in Austin TX -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, KVRX Austin" >> /home/$USER/.config/lttr/lttrrc;
                echo "http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause http://tstv-stream.tsm.utexas.edu:8000/kvrx_livestream --input-ipc-server=/tmp/mpvsocket;

elif [ "$username" = 9 ]; then
echo " -- Now you're listinging to WFUV, Straight Outta The Bronx! -- ";
		echo " ";
		echo "$comment0" > /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
                echo "$comment1" >> /home/$USER/.config/lttr/lttrrc ;
                echo "$comment2" >> /home/$USER/.config/lttr/lttrrc;
                echo "" >> /home/$USER/.config/lttr/lttrrc;
		echo "#EXTINF:-1, WFUV New York" >> /home/$USER/.config/lttr/lttrrc;
                echo "https://music.wfuv.org/music-hi" >> /home/$USER/.config/lttr/lttrrc;
		exec mpv --pulse-buffer=500 --cache=yes --cache-pause https://music.wfuv.org/music-hi --input-ipc-server=/tmp/mpvsocket;
fi

